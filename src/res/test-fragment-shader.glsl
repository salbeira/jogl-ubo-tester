#version 330

layout(std140) uniform Third{
	vec2 one;
	vec2 two;
	vec4 three;
};

layout(std140) uniform Fourth{
	vec2 four;
};

out vec4 out_color;

void main(){
	vec4 t_pos = gl_FragCoord;
	vec2 pos = vec2(t_pos.x, t_pos.y);
	if(pos.x < 200 && pos.y < 200){
		if(one != vec2(1,1)){
			out_color = vec4(1,0,0,1);
		} else {
			out_color = vec4(0,1,0,1);
		}
	}
	if(pos.x > 200 && pos.y < 200){
		if(two != vec2(2,2)){
			out_color = vec4(1,0,0,1);
		} else {
			out_color = vec4(0,1,0,1);
		}
	}
	if(pos.x < 200 && pos.y > 200){
		if(three != vec4(3,3,3,3)){
			out_color = vec4(1,0,0,1);
		} else {
			out_color = vec4(0,1,0,1);
		}
	}
	if(pos.x > 200 && pos.y > 200){
		if(four != vec2(4,4)){
			out_color = vec4(1,0,0,1);
		} else {
			out_color = vec4(0,1,0,1);
		}
	}
}