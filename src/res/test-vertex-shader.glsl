#version 330

//layout(std140) uniform First {
//	vec2 first;
//	vec2 second;
//	vec2 third;
//};

//layout(std140) uniform Second {
//	vec2 canvas_size;
//};

layout(location = 0) in vec2 vertex;

void main(){
	gl_Position = vec4 (vertex.x, vertex.y, -1, 1);
}