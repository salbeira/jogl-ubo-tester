package opengl;

import static javax.media.opengl.GL4.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.media.opengl.DebugGL4;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GL4;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JFrame;

import com.jogamp.opengl.util.Animator;

public class UniformBufferTestWindow extends JFrame implements GLEventListener{
	
	private static final long serialVersionUID = 42L;
	
	private IntBuffer ID_BUFFER;
	private GLCanvas canvas;
	private Animator animator;
	
	private int pid;
	private int vao;
	
	public UniformBufferTestWindow(){
		this.ID_BUFFER = IntBuffer.allocate(1);
		this.canvas = new GLCanvas();
		this.canvas.setSize(400,400);
		this.canvas.addGLEventListener(this);
		this.add(canvas);
		this.pack();
		this.animator = new Animator(this.canvas);
		this.setVisible(true);
		this.animator.start();
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL4 gl = drawable.getGL().getGL4();
		
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
		
		gl.glUseProgram(this.pid);
		gl.glBindVertexArray(this.vao);
		
		gl.glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, 0);
		
		gl.glBindVertexArray(0);
		gl.glUseProgram(0);
		
	}
	
	@Override
	public void dispose(GLAutoDrawable drawable) {}
	
	@Override
	public void init(GLAutoDrawable drawable) {
		GL4 gl = drawable.getGL().getGL4();
		gl = new DebugGL4(gl);
		System.out.println(gl.glGetString(GL_VERSION)); 
		/* Enable Texturing with Alpha-Values */
		gl.glEnable(GL_BLEND);
		gl.glBlendFunc(GL_SRC_ALPHA, GL4.GL_ONE_MINUS_SRC_ALPHA);
		/* Set Clear values */
		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		gl.glClearDepth(0);
		/* Enable Depth Test */
		gl.glEnable(GL_DEPTH_TEST);
		gl.glDepthFunc(GL_LEQUAL);
		
		this.pid = initializeShaderProgram(gl);
		
		initUniformBlocks(gl);
		
		this.vao = initVAO(gl);
		
		System.out.println(pid);
		System.out.println(vao);
	}
	
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {}
	
	private int initializeShaderProgram(GL4 gl){
		int vs = loadShader("res/test-vertex-shader.glsl", GL_VERTEX_SHADER, gl);
		int fs = loadShader("res/test-fragment-shader.glsl", GL_FRAGMENT_SHADER, gl);
		
		int pid = gl.glCreateProgram();
		gl.glAttachShader(pid, vs);
		gl.glAttachShader(pid, fs);
		
		gl.glLinkProgram(pid);
		gl.glValidateProgram(pid);
		
		ByteBuffer cbuffer = ByteBuffer.allocate(512);
		IntBuffer ibuffer = IntBuffer.allocate(16);
		gl.glGetProgramInfoLog(pid, 512, ibuffer, cbuffer);

		String infoLog = "";
		
		int c = 0;
		boolean written = false;
		for(int i = 0 ; i < 16 ; i++){
			for(int j = ibuffer.get(i) ; j > 0 ; j--){
				char l = (char) cbuffer.get(c);
				c++;
				infoLog += l;
				written = true;
			}
			if(written){
				infoLog += '\n';
				written = false;
			}
		}
		if(!infoLog.equals("")){
			System.err.println(infoLog);
		}
		
		return pid;
	}
	
	private int loadShader(String file, int shaderType, GL4 gl){
		String string = "";
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(loadResource(file)));
			String line;
			while((line = reader.readLine()) != null){
//DEBUG			System.out.println(line);
				string += line + "\n";
			}
		} catch (IOException e){
			e.printStackTrace();
			System.exit(-1);
		}
		
		int id = gl.glCreateShader(shaderType);
		gl.glShaderSource(id, 1, new String[] {string}, null);
		gl.glCompileShader(id);
		
		ByteBuffer cbuffer = ByteBuffer.allocate(1024);
		IntBuffer ibuffer = IntBuffer.allocate(16);
		gl.glGetShaderInfoLog(id, 1024, ibuffer, cbuffer);
		
		String infoLog = "";
		
		int c = 0;
		boolean written = false;
		for(int i = 0 ; i < 16 ; i++){
			for(int j = ibuffer.get(i) ; j > 0 ; j--){
				char l = (char) cbuffer.get(c);
				c++;
				infoLog += l;
				written = true;
			}
			if(written){
				if(!infoLog.endsWith("\n")){
					infoLog += '\n';
				}
				written = false;
			}
		}
		
		if(!infoLog.equals("")){
			System.err.println(infoLog);
		}
		
		return id;
	}
	
	private void initUniformBlocks(GL4 gl){
		int thirdIndex = gl.glGetUniformBlockIndex(pid, "Third");
		int fourthIndex = gl.glGetUniformBlockIndex(pid, "Fourth");
		
		int thirdBuffer = this.generateBuffer(gl);
		
		FloatBuffer floats = FloatBuffer.allocate(8);
		floats.put(new float[]{1,1,2,2,3,3,3,3});
		floats.flip();
		
		gl.glUniformBlockBinding(pid, thirdIndex, 3);
		gl.glBindBufferBase(GL_UNIFORM_BUFFER, 3, thirdBuffer);
		gl.glBufferData(GL_UNIFORM_BUFFER, 8 * 4, floats, GL_DYNAMIC_DRAW);
		
		floats = FloatBuffer.allocate(2);
		floats.put(new float[]{4,4});
		floats.flip();
		
		int fourthBuffer = this.generateBuffer(gl);
		
		gl.glUniformBlockBinding(pid, fourthIndex, 4);
		gl.glBindBufferBase(GL_UNIFORM_BUFFER, 4, fourthBuffer);
		gl.glBufferData(GL_UNIFORM_BUFFER, 2 * 4, floats, GL_DYNAMIC_DRAW);
	}
	
	private int initVAO(GL4 gl){
		int error;
		
		float[] vertices = {
				-1f, -1f,
				-1f, 1f,
				1f, 1f,
				1f, -1f,
				};

		
		FloatBuffer vBuffer = FloatBuffer.allocate(vertices.length);
		vBuffer.put(vertices);
		vBuffer.flip();
		
		byte[] indices = {
				0, 1, 2,
				2, 3, 0
		};
		
		ByteBuffer iBuffer = ByteBuffer.allocate(indices.length).order(ByteOrder.nativeOrder());
		iBuffer.put(indices);
		iBuffer.flip();
		
		int vaoID = this.generateVertexArray(gl);
		int vboID = this.generateBuffer(gl);
		int vboiID = this.generateBuffer(gl);
		
		if((error = gl.glGetError()) != 0){
			System.out.println("Error after setting up VAO or VBA: "+ error);
		} else {
			System.out.println("VBA and VBOs created");
		}
		
		/* Bind VAO */
		gl.glBindVertexArray(vaoID);
		/* Bind VBO - this adds the VBO to the VAO */
		gl.glBindBuffer(GL_ARRAY_BUFFER, vboID);
		/* Enable linking the just bound VBO to the input of a shader */
		gl.glEnableVertexAttribArray(0);
		gl.glVertexAttribPointer(0, 2, GL2.GL_FLOAT, false, 0, 0L);
		/* Buffer Data into the Buffer */
		gl.glBufferData(GL_ARRAY_BUFFER, vertices.length * 4, vBuffer, GL_STATIC_DRAW);
		/* Bind EBO - this adds the EBO to the VAO */
		gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboiID);
		/* Buffer Data into the Buffer */
		gl.glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.length, iBuffer, GL_STATIC_DRAW);
		/* UnBind for safety reasons ... */
		gl.glBindVertexArray(0);
		
		return vaoID;
	}
	
	private int generateBuffer(GL4 gl){
		gl.glGenBuffers(1, this.ID_BUFFER);
		return this.ID_BUFFER.get(0);
	}
	
	private int generateVertexArray(GL4 gl){
		gl.glGenVertexArrays(1, this.ID_BUFFER);
		return this.ID_BUFFER.get(0);
	}
	
	public static InputStream loadResource(String file) throws IOException {
		URL url = UniformBufferTestWindow.class.getClassLoader().getResource(file);
		if(url == null){
			throw new IOException("Cannot find: " + file);
		}
		return url.openStream();
	}
	
	public static void main(String... args){
		new UniformBufferTestWindow();
	}
}
